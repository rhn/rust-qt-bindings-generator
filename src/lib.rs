extern crate regex;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate serde_xml_rs;
extern crate toml;

pub mod build;
pub mod configuration;
mod configuration_private;
mod cpp;
mod rust;
mod util;

use std::error::Error;
use std::path::Path;
use configuration::Config;

/// Read a file with bindings.
pub fn read_bindings_file<P: AsRef<Path>>(
    config_file: P,
) -> Result<Config, Box<Error>> {
    configuration::parse(config_file)
}

/// Generate bindings from a bindings configuration.
pub fn generate_bindings<P: AsRef<Path>>(config: &Config, base_directory: P) -> Result<(), Box<Error>> {
    cpp::write_header(config, &base_directory)?;
    cpp::write_cpp(config, &base_directory)?;
    rust::write_interface(config, &base_directory)?;
    rust::write_implementation(config, &base_directory)?;
    Ok(())
}

/// Generate bindings from a bindings configuration file.
pub fn generate_bindings_from_config_file<P: AsRef<Path>, Q: AsRef<Path>>(
    config_file: P,
    overwrite_implementation: bool,
    base_directory: Option<Q>
) -> Result<(), Box<Error>> {
    let mut config = read_bindings_file(config_file)?;
    if overwrite_implementation {
        config.overwrite_implementation = true;
    }
    let base_dir = match base_directory {
        Some(path) => path.as_ref().to_path_buf(),
        None => config.config_file.parent().unwrap().to_path_buf()
    };
    generate_bindings(&config, base_dir)
}
